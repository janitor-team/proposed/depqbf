depqbf (5.01-3) unstable; urgency=medium

  * debian/tests: add dep8 testsuite

 -- Thomas Krennwallner <tkren@kr.tuwien.ac.at>  Sat, 05 May 2018 12:58:06 +0200

depqbf (5.01-2) unstable; urgency=medium

  * debian/gbp.conf: pristine-tar is True
  * debian/control: cleanup for Standards-Version 4.1.4
    - set Priority optional
    - Build-Depends on debhelper (>= 11)
    - use secure Homepage https://lonsing.github.io/depqbf/
    - update Vcs-* for https://salsa.debian.org/science-team/depqbf
  * fix DEB_BUILD_MAINT_OPTIONS and install changelog
    - debian/rules: add DEB_BUILD_MAINT_OPTIONS = hardening=+all
    - install NEWS as upstream changelog
  * debian/copyright: fix Upstream-Contact, use secure Format and Source
  * debian/compat: set to 11

 -- Thomas Krennwallner <tkren@kr.tuwien.ac.at>  Sat, 05 May 2018 09:46:34 +0200

depqbf (5.01-1) unstable; urgency=medium

  * New upstream version 5.01
  * bump Standards-Version to 3.9.8, no changes needed
  * debian/control: fix Vcs-Browser link
  * debian/depqbf.lintian-overrides: add non-dev-pkg-with-shlib-symlink

 -- Thomas Krennwallner <tkren@kr.tuwien.ac.at>  Sun, 27 Nov 2016 17:21:59 +0100

depqbf (5.0-1) unstable; urgency=medium

  * Imported Upstream version 5.0
  * refresh depqbf-makefile.patch
  * depqbf.lintian-overrides: add override for no-shlibs-control-file for
    internal libqdpll.so.1.0

 -- Thomas Krennwallner <tkren@kr.tuwien.ac.at>  Tue, 22 Dec 2015 09:17:35 +0100

depqbf (3.04-1) unstable; urgency=medium

  * Initial upstream branch.
  * Imported Upstream version 3.02 and 3.04.
  * debian/control:
    - Bump Standards-Version to 3.9.6.
    - Fix Vcs fields to use anonscm.debian.org.
  * debian/watch: Use github archive instead of deprecated
    githubredir.debian.net.

 -- Thomas Krennwallner <tkren@kr.tuwien.ac.at>  Sat, 04 Oct 2014 10:25:42 +0200

depqbf (3.02-1) unstable; urgency=low

  * New upstream release.
  * Add support for libqdpll.
  * debian/install: Install libqdpll.* and qdpll.h.
  * debian/docs: Add examples directory.
  * debian/links: Symlink libqdpll.so.1.0.
  * debian/patches/depqbf-makefile.patch: quilt refresh.

 -- Thomas Krennwallner <tkren@kr.tuwien.ac.at>  Thu, 24 Apr 2014 07:27:13 +0200

depqbf (3.01-1) unstable; urgency=low

  * New upstream release.

 -- Thomas Krennwallner <tkren@kr.tuwien.ac.at>  Sun, 20 Apr 2014 08:20:11 +0200

depqbf (3.0-1) unstable; urgency=low

  * New upstream release.
  * debian/control: Bump Standards-Version to 3.9.5.
  * debian/patches/depqbf-makefile.patch: quilt refresh.
  * debian/copyright: Fix years.

 -- Thomas Krennwallner <tkren@kr.tuwien.ac.at>  Thu, 20 Feb 2014 16:31:40 +0100

depqbf (2.0-1) unstable; urgency=low

  * New upstream release.
  * debian/patches/depqbf-makefile.patch: quilt refresh.
  * debian/compat: Bump version.
  * debian/control: Bump Standards-Version, Build-Depends.
  * debian/copyright: Update year.
  * debian/docs: Add NEWS.

 -- Thomas Krennwallner <tkren@kr.tuwien.ac.at>  Thu, 19 Sep 2013 12:36:40 +0200

depqbf (1.0-1) unstable; urgency=low

  * New upstream release.
  * debian/control: Bump Standards-Version and update Homepage and Description,
    Build-Depends on dpkg-dev.
  * debian/copyright: Update Format, Upstream-Contact, Source, and Copyright.
  * debian/watch: Scan github for version tags.
  * debian/patches/depqbf-makefile.patch: Add hardening support.
  * debian/rules: Enable dpkg-buildflags.

 -- Thomas Krennwallner <tkren@kr.tuwien.ac.at>  Sun, 12 Aug 2012 22:02:14 +0200

depqbf (0.1-2) unstable; urgency=low

  * debian/copyright: Fix unversioned-copyright-format-uri lintian
    warning.

 -- Thomas Krennwallner <tkren@kr.tuwien.ac.at>  Mon, 26 Sep 2011 08:33:16 +0200

depqbf (0.1-1) unstable; urgency=low

  * Initial release (Closes: #639363)

 -- Thomas Krennwallner <tkren@kr.tuwien.ac.at>  Fri, 26 Aug 2011 10:56:08 +0200
